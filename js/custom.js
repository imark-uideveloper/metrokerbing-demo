new WOW().init();

//input zoom js
document.documentElement.addEventListener('touchstart', function (event) {
    if (event.touches.length > 1) {
        event.preventDefault();
    }
}, false);


//first letter cap

jQuery('.firstCap').on('keypress', function (event) {
    var $this = jQuery(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});



/*gallery slider*/

     $(document).ready(function() {
              $('.galleryBlk .owl-carousel').owlCarousel({
                loop: false,
                  nav:true,
                  dots:false,
                margin: 0,
                responsiveClass: true,
                  
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 3

                  },
                  1000: {
                    items: 4
                  }
                }
              })
            })
     
     
/*testimonal*/
     
          $(document).ready(function() {
              $('.testimonalBlk .owl-carousel').owlCarousel({
                loop: false,
                  nav:true,
                  dots:false,
                margin: 20,
                responsiveClass: true,
                  
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 2

                  },
                  1000: {
                    items: 2
                  }
                }
              })
            })